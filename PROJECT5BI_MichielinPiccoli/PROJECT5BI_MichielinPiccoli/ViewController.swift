//
//  ViewController.swift
//  PROJECT5BI_MichielinPiccoli
//
//  Created by Alessio Vian on 22/11/2018.
//  Copyright © 2018 Michielin-Piccoli. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var inserimento: UITextField!
    @IBOutlet weak var risultato_lbl: UILabel!
    var arrayDecimali : [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func calcolaIP() {
        let binario = inserimento.text!
        if let decimale = Int(binario, radix: 2) {
            risultato_lbl.text = "\(decimale)"
        }
    }
    
    @IBAction func btn_controllo(_ sender: Any) {
        calcolaIP()
    }
    
}

